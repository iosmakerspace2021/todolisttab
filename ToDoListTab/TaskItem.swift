//
//  TaskItem.swift
//  ToDoListTab
//
//  Created by Olzhas Akhmetov on 22.03.2021.
//

import UIKit

class TaskItem: Codable {
    
    var task: String!
    var isComplete: Bool!
    
    init() {
        task = ""
        isComplete = false
    }
}
