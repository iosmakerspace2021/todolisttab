//
//  ViewController.swift
//  ToDoListTab
//
//  Created by Olzhas Akhmetov on 22.03.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func addTask(_ sender: Any) {
        let newTask = TaskItem()
        
        newTask.task = textField.text!
        
        textField.text = ""
        
        let defaults = UserDefaults.standard
        
        //defaults.removeObject(forKey: "taskArray")
        
        do {
            if let data = defaults.data(forKey: "taskArray") {
                
                var array = try JSONDecoder().decode([TaskItem].self, from: data)
                
                array.append(newTask)
                
                let encodedData = try JSONEncoder().encode(array)
                
                defaults.set(encodedData, forKey: "taskArray")
            } else {
                let encodedData = try JSONEncoder().encode([newTask])
                
                defaults.set(encodedData, forKey: "taskArray")
            }
        } catch {
            print("Unable to Encode Array (\(error))")
        }
        
        // OLD Code
//        if let array = defaults.array(forKey: "taskArray") {
//
//            var taskarray: [String] = array as! [String]
//
//            //taskarray.append(newTask)
//
//            defaults.set(taskarray, forKey: "taskArray")
//
//        } else {
//            defaults.set([newTask], forKey: "taskArray")
//        }
    }
    
}

